var express = require('express')
const fs = require('fs')

// EXERCÍCIO 26

/*
var argumentos = process.argv

var result = 0
for(const index of argumentos.keys()){
    
    if(index != 0 && index != 1){
        var valor = parseInt(argumentos[index])

        if(valor % 2 == 0){
            result += valor
        }
    }

}

console.log("A soma dos argumentos divisíveis por 2 é "+ result)
*/

// EXERCÍCIO 27

/*
var conteudo = {nome: "Herick Maia", idade: "21"}
conteudo = JSON.stringify(conteudo)

nome_arquivo = "info.json"

var opcao = {
    encoding: "utf8",
    flag: "a" //significa append, vai adicionar/acrescentar em um arquivo inexistente/existente
}

var meuArquivo = fs.writeFile(nome_arquivo, conteudo, opcao, (err) => {
    if(err){
        console.error(err)
    } else {
        console.log(conteudo + " gravado em "+ nome_arquivo)
    }
})
*/